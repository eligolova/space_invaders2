/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package space_invaders;

import java.util.Scanner;
import processing.core.PApplet;
/**
 *
 * @author eligolova
 */
public class Projectile_Joueur extends Projectile{
    
    
    Scanner in = new Scanner(System.in);
        
    Projectile_Joueur(JoueurTest v, Dessin d){
        longueur= 5;        
        tailleX=4;
        this.v=v;
        this.setPosInitX(v.getPosX()+(v.getTailleX()/2)-tailleX/2);
        this.setPosInitY(v.getPosY()-this.longueur);
        
        this.d=d;
        d.fill(d.color(0,255,0));
        d.rect(getPosInitX(), getPosInitY(), this.tailleX, this.longueur);
        
        //pour pouvoir supprimer les projectiles de la collection plus tard
        this.emplacement=d.projectiles.size();
        
        
}
    
    @Override
    public void Afficher(){
        d.fill(d.color(0,255,0));
        d.rect(this.getPosInitX(), this.getPosInitY(), this.tailleX, this.longueur);
        
        
}
    public void Deplacer(){
        //rect noir là ou le projectile était auparavant
        d.fill(0);
        d.rect(this.getPosInitX(), this.getPosInitY(), this.tailleX, this.longueur);  
        this.setPosInitY(this.getPosInitY() - longueur);
        if (d.get(this.getPosInitX(), this.getPosInitY()-longueur-2)!= d.color(0)){
            this.setPosInitY(this.getPosInitY() - this.longueur);        
            d.fill(0);
            d.rect(this.getPosInitX(), this.getPosInitY()-longueur, this.tailleX, this.longueur);  
            d.pMorts.add(this);
        }
            }   
    
    //effacer les projectiles morts
    public void Effacer(){
        d.fill(0);
        d.rect(this.getPosInitX(), this.getPosInitY(), this.tailleX, this.longueur);  
        
               
    }
    }
    
    

