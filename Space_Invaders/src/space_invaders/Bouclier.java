/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package space_invaders;

public class Bouclier {

    //toujours le même taille
    int tailleX;
    int tailleY = 50;
    int posX;
    int posY;
    Dessin d;

    Bouclier(int posX, int posY, Dessin d) {
        this.posX = posX;
        this.posY = posY;
        this.d = d;

        tailleX = d.getTailleX() / 6;
        tailleY = d.getTailleY() / 10;
        
        d.fill(0, 0, 255);
        d.stroke(125,125,125);
        d.rect(posX, posY, tailleX, tailleY);
    }
}
