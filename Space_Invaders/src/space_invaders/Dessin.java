/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package space_invaders;

import java.util.ArrayList;
import processing.core.PApplet;
import processing.core.PImage;

/**
 *
 * @author eligolova
 */
public class Dessin extends PApplet {

    //?? peût être pas nécessaire de les faire private??
    private int tailleX = 500;  //parametres pour la fenetre
    private int tailleY = 500;
    ArrayList<Alien> aliens = new ArrayList<>();    //création de collection vide pour les aliens
    ArrayList<Alien> aMorts = new ArrayList<>();   //aliens morts
    ArrayList<Projectile> projectiles = new ArrayList<>();
    ;  //collection de projectiles (d'alien + de joueur)
    ArrayList<Projectile> pMorts = new ArrayList<>();
    ;  //collection de projectiles (d'alien + de joueur)
    int rafraich = 48;
    int mouvements = 0;     //variable utilisé pour gerer la deplacement des aliens
    JoueurTest j1;
    int lowest_alien;       //alien le plus bas (pour la fenetre)
    int taille_aliens;

    public void setup() {


        frameRate(rafraich);

        //gestion de taille et couleur de fond de fenetre
        size(getTailleX(), getTailleY());
        background(0);

        //création de joueur
        //?? on definie la taille des boucliers, joueur et aliens comme static ou on le met dans le constructeur??
        j1 = new JoueurTest(getTailleX() / 2, 8 * getTailleY() / 9, 30, 30, this);

        //création de ligne de boucliers
        BouclierTest b1 = new BouclierTest(getTailleX() / 7, 3 * getTailleY() / 4, this);
        BouclierTest b2 = new BouclierTest(3 * getTailleX() / 7, 3 * getTailleY() / 4, this);
        BouclierTest b3 = new BouclierTest(5 * getTailleX() / 7, 3 * getTailleY() / 4, this);



        //variables utilisés pour la création des aliens
        int nb_aliens = 10; //nb aliens par ligne
        taille_aliens = tailleX / 15; //taille d'aliens
        int bordure = taille_aliens;    //distance entre bord et premier + dernier alien

        //lignes d'aliens avec ajoute en collection
        for (int i = 0; i < nb_aliens; i++) {

            //aliens crée colonne pas colonne
            int ligne = 1;
            aliens.add(new AlienTest2(i * getTailleX() / (nb_aliens + 2) + bordure, getTailleY() / 15, taille_aliens, taille_aliens, this));
            ligne++;
            aliens.add(new AlienTest(i * getTailleX() / (nb_aliens + 2) + 2 * bordure, ligne * getTailleY() / 15, taille_aliens, taille_aliens, this));
            ligne++;
            aliens.add(new AlienTest3(i * getTailleX() / (nb_aliens + 2) + bordure, ligne * getTailleY() / 15, taille_aliens, taille_aliens, this));
            ligne++;
            aliens.add(new AlienTest(i * getTailleX() / (nb_aliens + 2) + 2 * bordure, ligne * getTailleY() / 15, taille_aliens, taille_aliens, this));
            ligne++;
            aliens.add(new AlienTest2(i * getTailleX() / (nb_aliens + 2) + bordure, ligne * getTailleY() / 15, taille_aliens, taille_aliens, this));
            ligne++;
            aliens.add(new AlienTest(i * getTailleX() / (nb_aliens + 2) + 2 * bordure, ligne * getTailleY() / 15, taille_aliens, taille_aliens, this));
            ligne++;
            aliens.add(new AlienTest3(i * getTailleX() / (nb_aliens + 2) + bordure, ligne * getTailleY() / 15, taille_aliens, taille_aliens, this));
        }
        lowest_alien = 7 * getTailleY() / 15; //7 = nb de lignes d'aliens

    }

    public void draw() {

        //carré noir affiché pour effacer les aliens affiché auparavant
        //doit être fait pour que posY reste avec l'alien le plus bas pour n'avoir pas de problème avec les boucliers
        fill(0);
        noStroke();
        rect(0, 0, 500, lowest_alien + taille_aliens);

        // déjà créé en setup donc peût être il faut le rétirer soit d'ici soit de setup 
        //JoueurTest j1 = new JoueurTest(getTailleX() / 2, 7 * getTailleY() / 8, 30, 30, this);

        //rect noir affiché pour effacer le joueur affiché auparavant
        rect(0, getTailleY() - j1.getTailleX() * 2, getTailleY(), j1.getTailleY() + 2);

        //déplacement et affichage du joueur
        j1.Tirer();
        j1.Deplacer();
        j1.Afficher();

        //Afficher les aliens
        //je pense que utiliser un methode AfficherToutAliens en Alien sera mieux mais j'arrive pas à le faire marcher
        for (Alien alien : aliens) {
            if (frameCount % rafraich < rafraich / rafraich) {
                //Affichage des aliens avec un de deux images (bras en haut/en bas ou diff couleurs) dependant de valeur pair ou impair de framecount
                alien.Afficher();
                //changements de position des aliens
                alien.Deplacement();
            } else {
                alien.Afficher();
            }
            //pour changer la fenetre d'efacement si besoin
            if (alien.getPosY() > lowest_alien) {
                lowest_alien = alien.getPosY();
            }
        }
        //variable pour gerer les mouvements
        if (frameCount % rafraich < rafraich / rafraich) {
            Alien.mouvements++;
        }

        //reglements de projectiles
        for (Projectile p : projectiles) {
            p.Deplacer();

            p.Afficher();
        }

        //determiner quel aliens ont été touché pas les projectiles
        for (Alien alien : aliens) {
            for (Projectile p : pMorts) {
                System.out.println("mort");
                if (( (alien.getPosX()>=p.getPosInitX()) && (p.getPosInitX() <= (alien.getPosX() + alien.getTailleX()))) && ((p.getPosInitY() <= (alien.getPosY() + alien.getTailleY())) && p.getPosInitY() >= (alien.getPosY() + 10))) {
                    aMorts.add(alien);
                    System.out.println(alien.getTailleX());
                    System.out.println(alien.getPosX()+ " "+ alien.getPosY());
                }
            }
        }
            //effacer les aliens "morts"
            for (Alien morts : aMorts) {
                aliens.remove(morts);
            }

            for (Projectile p : pMorts) {
                p.Effacer();
                projectiles.remove(p);
            }
            //faire vide de nouveau
            aMorts.clear();
            pMorts.clear();

//        break; //ici pour n'avoir pas de boucle infinie mais aussi pour n'avoir pas à écrire si non pas vide plusierus fois dans la boucle

            // }


        
    }

    //+++++++++++++++++++accesseurs+++++++++++++++++++++++++++
    public int getTailleX() {
        return tailleX;
    }

    /**
     * @param tailleX the tailleX to set
     */
    public void setTailleX(int tailleX) {
        this.tailleX = tailleX;
    }

    /**
     * @return the tailleY
     */
    public int getTailleY() {
        return tailleY;
    }

    /**
     * @param tailleY the tailleY to set
     */
    public void setTailleY(int tailleY) {
        this.tailleY = tailleY;
    }
}
