/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package space_invaders;

/**
 *
 * @author eligolova
 */
public abstract class Projectile {
    
    Vaisseau v;
    Dessin d;
    int longueur;
    int tailleX;
    private int posX;    
    private int posY;    
    int emplacement;    //emplacement dans la collection projectiles
    
    public abstract void Afficher();
    public abstract void Deplacer();
    public abstract void Effacer();

    /**
     * @return the posX
     */
    public int getPosInitX() {
        return posX;
    }

    /**
     * @param posX the posX to set
     */
    public void setPosInitX(int posInitX) {
        this.posX = posInitX;
    }

    /**
     * @return the posY
     */
    public int getPosInitY() {
        return posY;
    }

    /**
     * @param posY the posY to set
     */
    public void setPosInitY(int posInitY) {
        this.posY = posInitY;
    }
}
