
package space_invaders;

import static space_invaders.Alien.mouvements;

/**
 *
 * @author eligolova
 */
public class AlienTest extends Alien {

    String pic = "alien_white_pos1.png";
    String pic2 = "alien_white_pos2.png";

    AlienTest(int posX, int posY, int tailleX, int tailleY, Dessin d) {

        // quand je retire ca du constructeur, il met chaque alien dans la même position quand ils bougent        
        setTailleX(tailleX);
        setTailleY(tailleY);
        setPosX(posX);
        setPosY(posY);
        this.d = d;
                
        //peût être on peut utiliser le constructeur de la classe mere ici au lieu de ce qui est en haut?
        //Alien(posX,posY,tailleX, tailleY);
                
        pos1 = d.loadImage(pic);
        pos1.resize(tailleX, tailleY);

        pos2 = d.loadImage(pic2);
        pos2.resize(tailleX, tailleY);

        d.image(pos1, posX, posY);

    }
    

    @Override
    public void Deplacement() {
        //changements de position horizontale des aliens
               if (mouvements == 16) {
            mouvements = 0;
        }
        if (mouvements == 8 | mouvements == 15) {
          this.setPosY(this.getPosY() + 5);}
        if (mouvements <= 7) {
            this.setPosX(this.getPosX() - 5);
        } else {
            this.setPosX(this.getPosX() + 5);
        }
    }

    }
    
    //*****************************************************************************
    
    //                              FIN
    
    
    //****************************pas la peine de régarder ca******************
    //MARCHE PAS????
//            public static void Afficher(Alien alien) {
//           // Dessin d= new Dessin();
//            d.image(alien.pos1, alien.getPosX(), alien.getPosY());
//            if (d.frameCount % d.rafraich < d.rafraich / 2) {
//                d.image(alien.pos1, alien.getPosX(), alien.getPosY());
//            } else {
//                d.image(alien.pos2, alien.getPosX(), alien.getPosY());
//            }

//        if (frameCount % rafraich < rafraich/2) image(pos1, posX, posY);
//        if (frameCount % rafraich > rafraich/2) image(pos1, posX, posY);
////      //  PImage pos1 = new PImage ("alien1_pos1.jpeg");
////        //PImage pos1 = new PImage (loadImage("alien1_pos1.jpeg"));

