/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package space_invaders;

import processing.core.PImage;

/**
 *
 * @author eligolova
 */
public class BouclierTest {
    int tailleX;
    int tailleY;
    int posX;
    int posY;
    Dessin d;
    
    PImage bouclier;

    BouclierTest(int posX, int posY, Dessin d) {
        this.posX = posX;
        this.posY = posY;
        this.d = d;

        tailleX = d.getTailleX() / 6;
        tailleY = d.getTailleY() / 10;
        
        bouclier = d.loadImage("mushroom_wall.jpg");
        
        //juste pour teste le mur de champignions
        bouclier.resize(tailleX, tailleY );
        //bouclier.resize(tailleX, tailleY );
        d.image(bouclier, posX, posY);
    }
    
}
