/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package space_invaders;

import processing.core.PImage;
import java.util.Scanner;
//import static space_invaders.Vaisseau.d;

/**
 *
 * @author eligolova
 */
public class JoueurTest extends Vaisseau {

    private PImage joueur;
    private int tailleX;
    private int tailleY;
    private int posX;
    private int posY;
    
// nécessaire pour la déplacement du joueur    
Scanner in=new Scanner(System.in);

    //constructeur de base
    JoueurTest() {
        //*******************ignore au début*********************
        // ?? peût être mieux d'utiliser ce constructeur au lieu du constructeur en bas ??
//        Dessin d = new Dessin();
//        this.d=d;
//        tailleX = d.getTailleX() / 10;
//        tailleY = d.getTailleY() / 10;
//
//        setPosX(d.getTailleX() / 2);
//        setPosY(7*d.getTailleY() / 8);
//
//
//        joueur = d.loadImage("joueur.jpg");
//        joueur.resize(tailleX, tailleY);
//        d.image(joueur, getPosX(), getPosY());
    }

    // constructeur du joueur
    // versatile pour permettra à la construction de plusieurs joueurs dans des positions et avec les tailles différentes *mais ?? peût être pas nécessaire vue que les paramétres sont toujours pareills ??
        JoueurTest(int posX, int posY, int tailleX, int tailleY, Dessin d) {
        this.tailleX = tailleX;
        this.tailleY = tailleY;
        setPosX(posX);
        setPosY(posY);
        this.d = d;

        joueur = d.loadImage("joueur.jpg");
        joueur.resize(tailleX, tailleY);
        d.image(joueur, posX, posY);

    }

    @Override
    public void Afficher() {
        d.image(this.getJoueur(), this.getPosX(), this.getPosY());
    }

    //Deplacement du joueur à gauche et à droit
    //??******************ne marche pas...
    public void Deplacer() {
        //deplacer à droit limite bord d'ecran + bordure
        if (d.keyCode == Dessin.RIGHT && (posX + 5) < d.getTailleX()-this.getTailleX()) {
            setPosX(this.getPosX() + 5);
        }
        //deplacer à gauche limite bord d'ecran + bordure
        if (d.keyCode == Dessin.LEFT && (posX - 5) > 0) {
            setPosX(this.getPosX() - 5);
        }
        //remette keyCode à rien pour deplacer le joueur seulement quand l'utilisateur appuye un bouton
        d.keyCode= Dessin.UP;
    }    
    
    public void Tirer() {
                if (d.key==' '){
            d.projectiles.add(new Projectile_Joueur(this, d));
            //pour ne continuer pas à tirer
            d.key='d';
                }
    }
    
    //+++++++++++++++++++accesseurs+++++++++++++++++++++++++++
    
    /**
     * @return the joueur
     */
    public PImage getJoueur() {
        return joueur;
    }

    /**
     * @param joueur the joueur to set
     */
    public void setJoueur(PImage joueur) {
        this.joueur = joueur;
    }

    /**
     * @return the tailleX
     */
    public int getTailleX() {
        return tailleX;
    }

    /**
     * @param tailleX the tailleX to set
     */
    public void setTailleX(int tailleX) {
        this.tailleX = tailleX;
    }

    /**
     * @return the tailleY
     */
    public int getTailleY() {
        return tailleY;
    }

    /**
     * @param tailleY the tailleY to set
     */
    public void setTailleY(int tailleY) {
        this.tailleY = tailleY;
    }

    /**
     * @return the posX
     */
    @Override
    public int getPosX() {
        return posX;
    }

    /**
     * @param posX the posX to set
     */
    @Override
    public void setPosX(int posX) {
        this.posX = posX;
    }

    /**
     * @return the posY
     */
    @Override
    public int getPosY() {
        return posY;
    }

    /**
     * @param posY the posY to set
     */
    public void setPosY(int posY) {
        this.posY = posY;
    }
}
