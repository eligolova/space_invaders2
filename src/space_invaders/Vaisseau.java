/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package space_invaders;

import processing.core.*;

/**
 *
 * @author eligolova
 */
public abstract class Vaisseau {
    
    Dessin d;

   // Vaisseau (int posX, int posY, int tailleX, int tailleY, Dessin d);
    
    
    abstract public void Afficher();
    
    // pas nécessaire si on reussi à faire la class projectiles
    //public void Tirer();
    
    
    //+++++++++++++++++++accesseurs+++++++++++++++++++++++++++
    /**
     * @return the posX
     */
    abstract public int getPosX() ;

    /**
     * @param posX the posX to set
     */
    abstract public void setPosX(int posX);

    /**
     * @return the posY
     */
    abstract public int getPosY();

    }

