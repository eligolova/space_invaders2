/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package space_invaders;

import processing.core.PImage;

/**
 *
 * @author eligolova
 */
public abstract class Alien extends Vaisseau {
    static int mouvements;
    private int tailleX;
    private int tailleY;
    private int posX;
    private int posY;
    
    //Dessin d = new Dessin();  je pense que ce n'est pas nécessaire mais je ne sais pas...
    
    //images differents pour que les aliens 'bougent les bras' ou changent couleur avec chaque mouvement
    PImage pos1;
    PImage pos2;

    //constructeur de base
    Alien() {
    }
    
    //constructeur generique
    Alien(int posX, int posY, int tailleX, int tailleY) {
        this.tailleX = tailleX;
        this.tailleY = tailleY;
        setPosX(posX);
        setPosY(posY);
        this.d = d;
    }
   
    @Override
        public void Afficher(){
                    if (d.frameCount % d.rafraich < d.rafraich / 2) {
                d.image(this.pos1, this.getPosX(), this.getPosY());
            } else {
                d.image(this.pos2, this.getPosX(), this.getPosY());
            }

    }
    
    public void Deplacement(){
        //changements de position horizontale des aliens
        if (mouvements == 16) {
            mouvements = 0;
        }
        if (mouvements == 8 | mouvements == 15) {
          this.setPosY(this.getPosY() + 10);}
        if (mouvements <= 7) {
            this.setPosX(this.getPosX() + 5);
        } else {
            this.setPosX(this.getPosX() - 5);
        }
    }

    //on peut faire un methode pour afficher tout les aliens!!
//   public void Afficher ( ArrayList<Alien> aliens){
//       for (Alien alien : aliens){
//           d.image(alien.pos1, posX, posY);   
//       }
    //**************************Ne marche pas!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//       public static void AfficherTout ( ArrayList<Alien> aliens){
//           Dessin d = new Dessin();
//           
//                   for (Alien alien : aliens){
//                         d.image(alien.pos1, alien.getPosX(), alien.getPosY()); 
//         if (d.frameCount % d.rafraich < d.rafraich / 2) {
//                d.image(alien.pos1, alien.getPosX(), alien.getPosY());
//            } else {
//                d.image(alien.pos2, alien.getPosX(), alien.getPosY());
//            }
//                   
//                   }
    
  

    
    //+++++++++++++++++++accesseurs+++++++++++++++++++++++++++

    /**
     * @return the tailleX
     */
    public int getTailleX() {
        return tailleX;
    }

    /**
     * @param tailleX the tailleX to set
     */
    public void setTailleX(int tailleX) {
        this.tailleX = tailleX;
    }

    /**
     * @return the tailleY
     */
    public int getTailleY() {
        return tailleY;
    }

    /**
     * @param tailleY the tailleY to set
     */
    public void setTailleY(int tailleY) {
        this.tailleY = tailleY;
    }

    /**
     * @return the posX
     */
    public int getPosX() {
        return posX;
    }

    /**
     * @param posX the posX to set
     */
    public void setPosX(int posX) {
        this.posX = posX;
    }

    /**
     * @return the posY
     */
    public int getPosY() {
        return posY;
    }

    /**
     * @param posY the posY to set
     */
    public void setPosY(int posY) {
        this.posY = posY;
    }
}
